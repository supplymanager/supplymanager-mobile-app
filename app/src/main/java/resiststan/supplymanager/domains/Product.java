package resiststan.supplymanager.domains;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
public class Product {
    private Long id;
    private String name;
    private String description;
    private Long time;
}
