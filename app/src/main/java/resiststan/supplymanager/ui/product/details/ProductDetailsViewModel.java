package resiststan.supplymanager.ui.product.details;

import android.app.Application;
import android.widget.Toast;

import javax.net.ssl.SSLSession;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import resiststan.supplymanager.domains.Product;
import resiststan.supplymanager.service.ProductService;
import resiststan.supplymanager.service.ServiceKeeper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsViewModel extends AndroidViewModel {
    private final ProductService productService = (ProductService) ServiceKeeper.getService("productService");

    private MutableLiveData<Product> product = new MutableLiveData<>();

    public ProductDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Product> getNewProduct() {
        product.setValue(new Product());

        return product;
    }

    public LiveData<Product> getExistProduct(long productId) {
        Call<Product> call = productService.getProductById(productId);

        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if (response.isSuccessful()) {
                    product.setValue(response.body());
                    return;
                }

                Toast.makeText(getApplication().getApplicationContext(), "Code: " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Toast.makeText(getApplication().getApplicationContext(), "Fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        return product;
    }

    public void createProduct(Product product) {
        Call<Product> call = productService.createProduct(product);

        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if (response.isSuccessful()) {
                    return;
                }

                Toast.makeText(getApplication().getApplicationContext(), "Code: " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Toast.makeText(getApplication().getApplicationContext(), "Fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public MutableLiveData<Product> getProduct() {
        return product;
    }

    public void updateProduct(long id, Product product) {
        Call<Product> call = productService.updateProduct(id, product);

        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if (response.isSuccessful()) {
                    return;
                }

                Toast.makeText(getApplication().getApplicationContext(), "Code: " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Toast.makeText(getApplication().getApplicationContext(), "Fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void deleteProduct(Long productId) {
        Call<Void> call = productService.deleteProduct(productId);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    return;
                }

                Toast.makeText(getApplication().getApplicationContext(), "Code: " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplication().getApplicationContext(), "Fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
