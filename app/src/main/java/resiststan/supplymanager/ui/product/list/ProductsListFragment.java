package resiststan.supplymanager.ui.product.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import resiststan.supplymanager.R;
import resiststan.supplymanager.domains.Product;
import resiststan.supplymanager.ui.product.details.ProductDetailsFragment;

public class ProductsListFragment extends Fragment {

    private ProductListViewModel productListViewModel;
    private NavController navController;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_products_list, container, false);
        initFab();

        final ProductAdapter productAdapter = new ProductAdapter();

        RecyclerView recyclerView = root.findViewById(R.id.mainRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(productAdapter);

        productListViewModel = new ViewModelProvider(this).get(ProductListViewModel.class);
        productListViewModel.allProducts().observe(getViewLifecycleOwner(), productAdapter::submitList);

        productAdapter.setOnItemClickListener(this::onItemClick);

//        addSwipe(productAdapter, recyclerView);

        return root;
    }

    private void initFab() {
        FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Bundle bundle = ProductDetailsFragment.getBundleCreateProduct();
            navController.navigate(R.id.productDetailsFragment, bundle);

            Toast.makeText(root.getContext(), "click add new", Toast.LENGTH_SHORT).show();
        });
    }

    private void addSwipe(ProductAdapter productAdapter, RecyclerView recyclerView) {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                productListViewModel.deleteProduct(productAdapter.getProductAt(viewHolder.getAdapterPosition()).getId());
            }
        }).attachToRecyclerView(recyclerView);
    }

    private void onItemClick(Product product) {
        Bundle bundle = ProductDetailsFragment.getBundleEditProduct(product.getId());
        navController.navigate(R.id.productDetailsFragment, bundle);

        Toast.makeText(root.getContext(), "click item", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
    }
}
