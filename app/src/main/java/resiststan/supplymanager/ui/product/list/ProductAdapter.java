package resiststan.supplymanager.ui.product.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import resiststan.supplymanager.R;
import resiststan.supplymanager.domains.Product;

public class ProductAdapter extends ListAdapter<Product, ProductAdapter.ProductHolder> {

    private final static int LIST_ITEM_LAYOUT_RES = R.layout.item_product;
    private final static DiffUtil.ItemCallback<Product> DIFF_CALLBACK = new DiffUtil.ItemCallback<Product>() {
        @Override
        public boolean areItemsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return Objects.equals(oldItem.getName(), newItem.getName())
                    && Objects.equals(oldItem.getDescription(), newItem.getDescription())
                    && Objects.equals(oldItem.getTime(), newItem.getTime())
                    ;
        }
    };

    private OnItemClickListener listener;

    public ProductAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(LIST_ITEM_LAYOUT_RES, parent, false);

        return new ProductHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        holder.bind(getItem(position));
    }

    public Product getProductAt(int position) {
        return getItem(position);
    }

    class ProductHolder extends RecyclerView.ViewHolder {

        private final TextView nameTextView;
        private final TextView descriptionTextView;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.tv_item_product_name);
            descriptionTextView = itemView.findViewById(R.id.tv_item_product_description);

            itemView.setOnClickListener(view -> onItemClick());
        }

        private void onItemClick() {
            int position = getAdapterPosition();
            if (listener != null && position != RecyclerView.NO_POSITION) {
                listener.onItemClick(getItem(position));
            }
        }

        private void bind(Product product) {
            nameTextView.setText(product.getName());
            descriptionTextView.setText(product.getDescription());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Product product);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}