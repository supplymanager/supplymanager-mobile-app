package resiststan.supplymanager.ui.product.list;

import android.app.Application;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import resiststan.supplymanager.domains.Product;
import resiststan.supplymanager.service.ProductService;
import resiststan.supplymanager.service.ServiceKeeper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListViewModel extends AndroidViewModel {

    private final ProductService productService = (ProductService) ServiceKeeper.getService("productService");

    private MutableLiveData<List<Product>> allProducts;


    public ProductListViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Product>> allProducts() {
        if (allProducts == null) {
            allProducts = new MutableLiveData<>();

            loadProducts();
        }

        return allProducts;
    }

    private void loadProducts() {
        Call<List<Product>> call = productService.getAllProducts();

        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if (response.isSuccessful()) {
                    allProducts.setValue(response.body());
                    return;
                }

                Toast.makeText(getApplication().getApplicationContext(), "Code: " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(getApplication().getApplicationContext(), "Fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void deleteProduct(Long productId) {
        Call<Void> call = productService.deleteProduct(productId);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    loadProducts();
                    Toast.makeText(getApplication().getApplicationContext(), "Product deleted", Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(getApplication().getApplicationContext(), "Code: " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplication().getApplicationContext(), "Fail: " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
