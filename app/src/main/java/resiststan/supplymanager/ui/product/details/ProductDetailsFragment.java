package resiststan.supplymanager.ui.product.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import resiststan.supplymanager.R;
import resiststan.supplymanager.domains.Product;

public class ProductDetailsFragment extends Fragment {
    private final static String EXTRA_KEY_PRODUCT_ID = "resiststan.supplymanager.ui.product.details.EXTRA_KEY_PRODUCT_ID";
    private final static String EXTRA_KEY_ACTION = "resiststan.supplymanager.ui.product.details.EXTRA_KEY_ACTION";
    private final static int CREATE_ACTION_CODE = 1;
    private final static int EDIT_ACTION_CODE = 2;

    private ProductDetailsViewModel productDetailsViewModel;
    private NavController navController;
    private View root;

    private EditText nameTextView;
    private EditText descriptionTextView;
    private EditText timeTextView;

    public static Bundle getBundleEditProduct(long productId) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_KEY_ACTION, EDIT_ACTION_CODE);
        bundle.putLong(EXTRA_KEY_PRODUCT_ID, productId);

        return bundle;
    }

    public static Bundle getBundleCreateProduct() {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_KEY_ACTION, CREATE_ACTION_CODE);

        return bundle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_details_product, container, false);

        initComponents();
        initViewModel();

        return root;
    }

    private void initComponents() {
        nameTextView = root.findViewById(R.id.tx_name);
        descriptionTextView = root.findViewById(R.id.tx_description);
        timeTextView = root.findViewById(R.id.tx_time);
    }

    private void initViewModel() {
        productDetailsViewModel = new ViewModelProvider(this).get(ProductDetailsViewModel.class);

        if (getActionCode() == EDIT_ACTION_CODE) {
            productDetailsViewModel.getExistProduct(getProductId()).observe(getViewLifecycleOwner(), this::updateUI);
        }
    }

    private void updateUI(Product product) {
        if (product != null) {
            nameTextView.setText(product.getName());
            descriptionTextView.setText(product.getDescription());
            timeTextView.setText(product.getTime() == null ? "" : String.valueOf(product.getTime()));
        }
    }

    private long getProductId() {
        if (getArguments() != null && getArguments().containsKey(EXTRA_KEY_PRODUCT_ID)) {
            return getArguments().getLong(EXTRA_KEY_PRODUCT_ID);
        }

        return -1;
    }

    private int getActionCode() {
        if (getArguments() != null && getArguments().containsKey(EXTRA_KEY_ACTION)) {
            return getArguments().getInt(EXTRA_KEY_ACTION);
        }

        return -1;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.findItem(R.id.action_settings).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                saveProduct();
                break;

            case R.id.action_delete:
                deleteProduct();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveProduct() {
        Product product = new Product();
        product.setName(nameTextView.getText().toString());
        product.setDescription(descriptionTextView.getText().toString());
        product.setTime(Long.parseLong(timeTextView.getText().toString()));

        if (getActionCode() == EDIT_ACTION_CODE) {
            long id = getArguments().getLong(EXTRA_KEY_PRODUCT_ID);
            product.setId(id);
            productDetailsViewModel.updateProduct(id, product);
        } else {
            productDetailsViewModel.createProduct(product);
        }

        Toast.makeText(getContext(), "Press button save", Toast.LENGTH_SHORT).show();
        navController.navigate(R.id.action_productDetailsFragment_to_nav_product);
    }

    private void deleteProduct() {
        long id = getArguments().getLong(EXTRA_KEY_PRODUCT_ID);

        productDetailsViewModel.deleteProduct(id);

        Toast.makeText(getContext(), "Press button delete", Toast.LENGTH_SHORT).show();
        navController.navigate(R.id.action_productDetailsFragment_to_nav_product);
    }
}
