package resiststan.supplymanager.service;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import resiststan.supplymanager.api.SupplyManagerAPI;
import resiststan.supplymanager.domains.Product;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductService implements Service {

    private HttpLoggingInterceptor httpLoggingInterceptor;
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private SupplyManagerAPI supplyManagerAPI;

    public ProductService() {
        httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(SupplyManagerAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        supplyManagerAPI = retrofit.create(SupplyManagerAPI.class);
    }

    public Call<List<Product>> getAllProducts() {
        return supplyManagerAPI.getAllProducts();
    }

    public Call<Product> getProductById(long productId) {
        return supplyManagerAPI.getProductById(productId);
    }

    public Call<Void> deleteProduct(long productId) {
        return supplyManagerAPI.deleteProduct(productId);
    }

    public Call<Product> createProduct(Product product) {
        return supplyManagerAPI.createProduct(product);
    }

    public Call<Product> updateProduct(long id, Product product) {
        return supplyManagerAPI.updateProduct(id, product);
    }
}
