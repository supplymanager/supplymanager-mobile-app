package resiststan.supplymanager.service;

import java.util.HashMap;
import java.util.Map;

public class ServiceKeeper {
    private final static Map<String, Service> services = new HashMap<>();

    static {
        ProductService productService = new ProductService();

        services.put("productService", productService);
    }

    public static Service getService(String name) {
        return services.get(name);
    }
}
